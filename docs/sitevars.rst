.. |release| replace:: 3006.3
.. |supported-release-2| replace:: 3005.3
.. |supported-release-1-badge| replace:: :bdg-link-success:`3006.3 <https://docs.saltproject.io/en/latest/topics/releases/3006.3.html>`
.. |supported-release-2-badge| replace:: :bdg-link-primary:`3005.3 <https://docs.saltproject.io/en/3005/topics/releases/3005.3.html>`

.. |quickstart-script-path| replace:: https://github.com/saltstack/salt-bootstrap/blob/develop/salt-quick-start.sh
.. |quickstart-script-path-windows| replace:: https://github.com/saltstack/salt-bootstrap/blob/develop/salt-quick-start.ps1

.. |release-candidate-version| replace:: 3006.0rc3
.. |debian-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/ bullseye main"
.. |rhel-release-candidate-gpg| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel-release-candidate| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel-release-candidate-echo| replace:: 'baseurl=https://repo.saltproject.io/salt_rc/salt/py3/redhat/$releasever/$basearch/latest'
.. |ubuntu-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/ jammy main"
.. |bootstrap-release-candidate| replace:: python3 git v3006.0rc3
.. |pip-install-release-candidate| replace:: sudo pip install salt==3006.0rc3

.. |amazon-linux2-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/3006.repo
.. |amazon-linux2-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/minor/3006.3.repo

.. |amazon-linux2-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/latest.repo
.. |amazon-linux2-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/3006.repo
.. |amazon-linux2-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/minor/3006.3.repo

.. |centos9-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |centos9-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3006.repo
.. |centos9-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3006.3.repo

.. |centos9-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/latest.repo
.. |centos9-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/3006.repo
.. |centos9-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/minor/3006.3.repo

.. |centos8-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |centos8-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3006.repo
.. |centos8-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3006.3.repo

.. |centos8-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/latest.repo
.. |centos8-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/3006.repo
.. |centos8-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/minor/3006.3.repo

.. |centos7-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |centos7-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3006.repo
.. |centos7-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3006.3.repo

.. |centos7-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/latest.repo
.. |centos7-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/3006.repo
.. |centos7-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/minor/3006.3.repo

.. |debian11-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/latest bullseye main
.. |debian11-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/3006 bullseye main
.. |debian11-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/minor/3006.3 bullseye main

.. |debian11-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/latest bullseye main
.. |debian11-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/3006 bullseye main
.. |debian11-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/minor/3006.3 bullseye main

.. |debian10-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/latest buster main
.. |debian10-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/3006 buster main
.. |debian10-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/minor/3006.3 buster main

.. |debian10-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/latest buster main
.. |debian10-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/3006 buster main
.. |debian10-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/minor/3006.3 buster main

.. |fedora38-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/latest.repo
.. |fedora38-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/3006.repo
.. |fedora38-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/minor/3006.3.repo

.. |fedora38-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/latest.repo
.. |fedora38-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/3006.repo
.. |fedora38-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/minor/3006.3.repo

.. |fedora37-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/latest.repo
.. |fedora37-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/3006.repo
.. |fedora37-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/minor/3006.3.repo

.. |fedora37-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/latest.repo
.. |fedora37-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/3006.repo
.. |fedora37-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/minor/3006.3.repo

.. |macos-amd64-download| replace:: https://repo.saltproject.io/salt/py3/macos/latest/salt-3006.3-py3-x86_64.pkg
.. |macos-amd64-gpg| replace:: https://repo.saltproject.io/salt/py3/macos/SALT-PROJECT-GPG-PUBKEY-2023.gpg

.. |photonos4-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest.repo
.. |photonos4-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/3006.repo
.. |photonos4-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/minor/3006.3.repo

.. |photonos3-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest.repo
.. |photonos3-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/3006.repo
.. |photonos3-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/minor/3006.3.repo

.. |rhel9-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel9-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3006.repo
.. |rhel9-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3006.3.repo

.. |rhel9-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/latest.repo
.. |rhel9-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/3006.repo
.. |rhel9-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/minor/3006.3.repo

.. |rhel8-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |rhel8-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3006.repo
.. |rhel8-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3006.3.repo

.. |rhel8-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/latest.repo
.. |rhel8-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/3006.repo
.. |rhel8-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/minor/3006.3.repo

.. |rhel7-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |rhel7-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3006.repo
.. |rhel7-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3006.3.repo

.. |rhel7-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/latest.repo
.. |rhel7-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/3006.repo
.. |rhel7-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/minor/3006.3.repo

.. |ubuntu22-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/latest jammy main
.. |ubuntu22-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/3006 jammy main
.. |ubuntu22-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/minor/3006.3 jammy main

.. |ubuntu22-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/latest jammy main
.. |ubuntu22-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/3006 jammy main
.. |ubuntu22-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/minor/3006.3 jammy main

.. |ubuntu20-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/3006 focal main
.. |ubuntu20-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/minor/3006.3 focal main

.. |ubuntu20-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/latest focal main
.. |ubuntu20-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/3006 focal main
.. |ubuntu20-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/minor/3006.3 focal main

.. |windows-install-exe-example| replace:: Salt-Minion-3006.3-Py3-AMD64-Setup.exe
.. |windows-install-msi-example| replace:: Salt-Minion-3006.3-Py3-AMD64.msi

.. |windows-amd64-exe-gpg| replace:: https://repo.saltproject.io/salt/py3/windows/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |windows-amd64-exe-download| replace:: https://repo.saltproject.io/salt/py3/windows/latest/Salt-Minion-3006.3-Py3-AMD64-Setup.exe
