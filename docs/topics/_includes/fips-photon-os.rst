.. Warning::

   The 3006 version of Salt for Photon OS is not yet compliant with FIPS.

   If your organization requires FIPS compliance and you need to install Salt on
   Photon OS 3 or 4, install the 3005 version of Salt instead. Although 3005 did
   not officially support Photon OS 4, Salt users have reported they have
   successfully used 3005 with Photon OS 4. See
   `Photon OS 3005 instructions <https://docs.saltproject.io/salt/install-guide/en/3005/topics/install-by-operating-system/photonos.html>`_
   for more information.

   Note that the FIPS-compliant version of Salt v3005 for Photon OS 3 and 4 are
   not onedir installations. See :ref:`what-is-onedir` for more information.
